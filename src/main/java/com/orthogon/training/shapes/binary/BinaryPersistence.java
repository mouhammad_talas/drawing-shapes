/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.binary;

import com.orthogon.training.shapes.IPersistence;
import com.orthogon.training.shapes.Layer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author mota
 */
public class BinaryPersistence implements IPersistence
{
   private List<Layer> binaryLayers = new ArrayList<>();
    
    
    @Override
    public String getExtension ()
    {
        return "ser";
    }

    @Override
    public void save (List<Layer> layers, File file)
    {
         try
        {

            FileOutputStream fos = new FileOutputStream(file);
            ObjectOutputStream oos = new ObjectOutputStream(fos);
            oos.writeObject(layers);
            oos.close();
            fos.close();

        }
        catch (IOException ioe)
        {
            LOG.log(Level.WARNING, "Failed to save to Ser file <{0}>.", file.getName());            
        }
    }

    @Override
    public List<Layer> load (File file)
    {
        String filePath = file.getPath();
        try
        {
            FileInputStream fis = new FileInputStream(filePath);
            ObjectInputStream ois = new ObjectInputStream(fis);
            
            binaryLayers = (List<Layer>) ois.readObject();
            ois.close();
            fis.close();
            

        }
        catch (IOException | ClassNotFoundException ioe)
        {
            LOG.log(Level.WARNING, "Failed to read from Ser file <{0}>.", file.getName());
            ioe.toString();
        }
        
      return binaryLayers;
    }

  
    
}
