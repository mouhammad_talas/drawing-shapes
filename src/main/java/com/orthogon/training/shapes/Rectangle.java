/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Point;
import java.util.UUID;

/**
 *
 */
public class Rectangle extends Shape
{
   private int rectangleX = getxFirstPosition();
   private int rectangleY = getyFirstPosition();
   private final int rectangleW = 200;
   private final int rectangleH = 100;
   private Color currentColor;
   private Point coordinates;
   
   /**
    * 
    */
   public Rectangle ()
   {
      super();
   }
   
   /**
    * 
    * @param id 
    */
   public Rectangle (UUID id)
   {
      super(id);
   }
   
    /**
    * 
    * @return the position of x 
    */
    public int getRectangleX ()
    {
        return rectangleX;
    }

    /**
     * 
     * @param rectangleX represents the position of x 
     */
    public void setRectangleX (int rectangleX)
    {
        this.rectangleX = rectangleX;
    }
    
    /**
     * 
     * @return the position of y
     */
    public int getRectangleY ()
    {
        return rectangleY;
    }

    /**
     * 
     *@param rectangleY represents the position of y
     */
    public void setRectangleY (int rectangleY)
    {
        this.rectangleY = rectangleY;
    }
    
    /**
     * 
     * @return the width
     */
    public int getRectangleW ()
    {
        return rectangleW;
    }
    
    
    /**
     * 
     * @return the height 
     */
    public int getRectangleH ()
    {
        return rectangleH;
    }

    
    @Override
    public boolean isSelected (int x, int y)
    {
       return x <= rectangleX + rectangleW && y <= rectangleY + rectangleH && x >= rectangleX && y >= rectangleY;
    }

    @Override
    public void move (int x, int y)
    {
        rectangleX += x;
        rectangleY += y;
        
        setCoordinates(getRectangleX(), getRectangleY());

    }

    @Override
    public void draw (Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;

        
        g2.setColor(currentColor);
        g2.fillRect(rectangleX, rectangleY, rectangleW, rectangleH);
        g2.setColor(Color.black);
        g2.drawRect(rectangleX, rectangleY, rectangleW, rectangleH);
    }

    @Override
    public void setcolor (Color color)
    {
        currentColor = color;
    }



    

    
}
