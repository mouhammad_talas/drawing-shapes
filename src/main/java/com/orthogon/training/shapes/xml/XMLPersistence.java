/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.xml;

import com.orthogon.training.shapes.IPersistence;
import com.orthogon.training.shapes.Layer;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.AbstractList;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.logging.Level;

/**
 *
 */
public class XMLPersistence implements IPersistence
{
    List<Layer> xmlLayers = new ArrayList<>();
    
    
    
    @Override
    public String getExtension ()
    {
        return "xml";
    }

    @Override
    public void save (List<Layer> layers, File file)
    {

        try
        {

            java.beans.XMLEncoder fos = new java.beans.XMLEncoder(new FileOutputStream(file));
            fos.writeObject(layers);
            fos.flush();
            fos.close();

        }
        catch (IOException ioe)
        {
            LOG.log(Level.WARNING, "Failed to save to Xml file <{0}>.", file.getName());
        }

    }

    @Override
    public List<Layer> load (File file)
    {
        String filePath = file.getPath();
        try
        {

            FileInputStream fis = new FileInputStream(filePath);
            java.beans.XMLDecoder d = new java.beans.XMLDecoder(fis);

            xmlLayers = (List<Layer>) d.readObject();

            d.close();
            fis.close();

        }

        catch (IOException ioe)
        {
            LOG.log(Level.WARNING, "Failed to read from Xml file <{0}>.", file.getName());
            ioe.toString();

        }
        return xmlLayers;
    }

}
