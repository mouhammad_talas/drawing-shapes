/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author mota
 */
public class DeleteMessage implements Serializable
{
    private UUID id;

    /**
     *
     * @param id unique id
     */
    public DeleteMessage (UUID id)
    {
        this.id = id;
    }

    /**
     *
     * @return id
     */
    public UUID getId ()
    {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId (UUID id)
    {
        this.id = id;
    }

}
