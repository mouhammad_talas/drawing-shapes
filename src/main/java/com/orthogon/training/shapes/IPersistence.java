/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.io.File;
import java.util.List;
import java.util.logging.Logger;

/**
 * Interface for loading and saving shape classes.
 */
public interface IPersistence
{
    /**
     *
     */
    Logger LOG = Logger.getLogger(MainPanel.class.getName());

    /**
     * get the file extension.
     *
     * @return a String representing the file extension.
     */
    String getExtension ();

    /**
     *
     * saving the the drawn Shapes to the given file.
     *
     * @param layers represent the layers that include the elements(shapes).
     * @param file the file to save the shapes to.
     */
    void save (List<Layer> layers, File file);

    /**
     * Read the scene graph from the given file.
     *
     * @param file the file to load the shapes from.
     * @return a list of layers.
     */
    List<Layer> load (File file);
}
