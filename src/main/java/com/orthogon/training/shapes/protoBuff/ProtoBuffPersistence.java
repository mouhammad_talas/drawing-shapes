/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.protoBuff;

import com.orthogon.training.shapes.Circle;
import com.orthogon.training.shapes.Diamond;
import com.orthogon.training.shapes.IPersistence;
import com.orthogon.training.shapes.Layer;
import com.orthogon.training.shapes.Oval;
import com.orthogon.training.shapes.PBHull;
import com.orthogon.training.shapes.Rectangle;
import com.orthogon.training.shapes.Square;
import com.orthogon.training.shapes.Triangle;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

/**
 *
 * @author mota
 */
public class ProtoBuffPersistence implements IPersistence
{
    private int layerIndex;
    

    @Override
    public String getExtension ()
    {
        return "Pbuf";
    }

    @Override
    public void save (List<Layer> layers, File file)
    {

        layerIndex = 0;
        PBHull.PBContainer.Builder containerBuilder = PBHull.PBContainer.newBuilder();
        layers.stream().forEach(layer ->
        {
            PBHull.PBLayer.Builder layerBuilder = PBHull.PBLayer.newBuilder();
            layerBuilder.setIndex(layerIndex);
            layer.getElements().forEach((key, shape) ->
            {
                if (shape instanceof Circle)
                {
                    Circle circle = (Circle) shape;
                    PBHull.PBCircle.Builder circleBuilder = PBHull.PBCircle.newBuilder();
                    circleBuilder.setX(circle.getCircleX())
                        .setY(circle.getCircleY())
                        .setRadius(circle.getCircleW());

                    layerBuilder.addCircles(circleBuilder);
                }
                else if (shape instanceof Square)
                {
                    Square square = (Square) shape;
                    PBHull.PBSquare.Builder squareBuilder = PBHull.PBSquare.newBuilder();
                    squareBuilder.setX(square.getSquareX())
                        .setY(square.getSquareY())
                        .setW(square.getSquareW())
                        .setH(square.getSquareH());

                    layerBuilder.addSquares(squareBuilder);
                }
                else if (shape instanceof Rectangle)
                {
                    Rectangle rectangle = (Rectangle) shape;
                    PBHull.PBRectangle.Builder rectangleBuilder = PBHull.PBRectangle.newBuilder();
                    rectangleBuilder.setX(rectangle.getRectangleX())
                        .setY(rectangle.getRectangleY())
                        .setH(rectangle.getRectangleH())
                        .setW(rectangle.getRectangleW());

                    layerBuilder.addRectangles(rectangleBuilder);
                }
                else if (shape instanceof Oval)
                {
                    Oval oval = (Oval) shape;
                    PBHull.PBOval.Builder ovalBuilder = PBHull.PBOval.newBuilder();
                    ovalBuilder.setX(oval.getOvalX())
                        .setY(oval.getOvalY())
                        .setW(oval.getOvalW())
                        .setH(oval.getOvalH());

                    layerBuilder.addOvals(ovalBuilder);

                }
                else if (shape instanceof Triangle)
                {
                    Triangle triangle = (Triangle) shape;
                    PBHull.PBTriangle.Builder triangleBuilder = PBHull.PBTriangle.newBuilder();

                    PBHull.Coordinate.Builder coordinateBuilder = PBHull.Coordinate.newBuilder();

                    for (int i = 0; i < triangle.getTriangleX().length; i++)
                    {
                        coordinateBuilder.setX(triangle.getTriangleX()[i]);
                        coordinateBuilder.setY(triangle.getTriangleY()[i]);
                        triangleBuilder.addCoordinates(i, coordinateBuilder);
                    }

                    layerBuilder.addTriangles(triangleBuilder);
                }
                else if (shape instanceof Diamond)
                {
                    Diamond diamond = (Diamond) shape;
                    PBHull.PBDiamond.Builder diamondBuilder = PBHull.PBDiamond.newBuilder();
                    
                    PBHull.Coordinate.Builder coordinateBuilder = PBHull.Coordinate.newBuilder();

                    for (int i = 0; i < diamond.getDiamondX().length; i++)
                    {
                        coordinateBuilder.setX(diamond.getDiamondX()[i]);
                        coordinateBuilder.setY(diamond.getDiamondY()[i]);
                        diamondBuilder.addCoordinates(i, coordinateBuilder);
                    }
                    
                    layerBuilder.addDiamonds(diamondBuilder);
                }
            });           
            containerBuilder.addLayers(layerBuilder);
            layerIndex++;
        });
        PBHull.PBContainer pbContainer = containerBuilder.build();
        try
        {
            pbContainer.writeTo(new FileOutputStream(file));
        }
        catch (IOException ex)
        {
            LOG.log(Level.WARNING, "Failed to save to ProtoBuf file <{0}>.", file.getName());
        }
    }

    @Override
    public List<Layer> load (File file)
    {
        List<Layer> layers = new ArrayList<>();
        
        PBHull.PBContainer.Builder containerBuilder = PBHull.PBContainer.newBuilder();
        try
        {
            containerBuilder.mergeFrom(new FileInputStream(file));
        }
        catch (IOException ex)
        {
            LOG.log(Level.WARNING, "Failed to read from ProtoBuf file <{0}>.", file.getName());
            
        }
        PBHull.PBContainer pbContainer = containerBuilder.build();
        
        
        for (int i = 0; i < pbContainer.getLayersCount(); i++)
        {
            layers.add(new Layer());
        }
        
        pbContainer.getLayersList().stream()
            .filter(pbLayer -> pbLayer.getIndex() < layers.size())
            .forEach(pbLayer ->
            {
                Layer layer = layers.get(pbLayer.getIndex());
                pbLayer.getCirclesList().stream().forEach(pbCircle ->
                {
                    Circle circle = new Circle();
                    circle.setCircleX(pbCircle.getX());
                    circle.setCircleY(pbCircle.getY());
                    layer.add(circle);
                });
                pbLayer.getSquaresList().stream().forEach(pbSquare ->
                {
                    Square square = new Square();
                    square.setSquareX(pbSquare.getX());
                    square.setSquareY(pbSquare.getY());
                    layer.add(square);
                });
                pbLayer.getRectanglesList().stream().forEach((pbRectangle) ->
                {
                    Rectangle rectangle = new Rectangle();
                    rectangle.setRectangleX(pbRectangle.getX());
                    rectangle.setRectangleY(pbRectangle.getY());
                    layer.add(rectangle);
                });
                pbLayer.getOvalsList().stream().forEach((pbOval) ->
                {
                    Oval oval = new Oval();
                    oval.setOvalX(pbOval.getX());
                    oval.setOvalY(pbOval.getY());
                    layer.add(oval);
                });
                pbLayer.getTrianglesList().stream().forEach((pbTriangle) ->
                {
                    Triangle triangle = new Triangle();

                    for (int i = 0; i < triangle.getTriangleX().length; i++)
                    {
                        triangle.setTriangleX(pbTriangle.getCoordinates(i).getX(), i);
                        triangle.setTriangleY(pbTriangle.getCoordinates(i).getY(), i);
                    }

                    layer.add(triangle);
                });
                pbLayer.getDiamondsList().stream().forEach((pbDiamond) ->
                {
                    Diamond dimaond = new Diamond();
                    
                    for (int i = 0; i < dimaond.getDiamondX().length; i++)
                    {
                        dimaond.setDiamondX(pbDiamond.getCoordinates(i).getX(), i);
                        dimaond.setDiamondY(pbDiamond.getCoordinates(i).getY(), i);
                    }
                    
                    layer.add(dimaond);
                });
            });
        return layers;
    }

   

}
