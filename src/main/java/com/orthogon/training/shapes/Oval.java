/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.UUID;

/**
 *
 */
public class Oval extends Shape
{
   private int ovalX = getxFirstPosition();
   private int ovalY = getyFirstPosition();
   private final int ovalW = 100;
   private final int ovalH = 150;
   private Color currentColor;
 
   /**
    * 
    */
   public Oval ()
   {
      super();
   }
   
   /**
    * 
    * @param id 
    */
   public Oval (UUID id)
   {
      super(id);
   }
   /**
    * 
    * @return get the x position
    */
    public int getOvalX ()
    {
        return ovalX;
    }

    /**
     * 
     * @param ovalX x position
     */
    public void setOvalX (int ovalX)
    {
        this.ovalX = ovalX;
    }

    /**
     * 
     * @return get the y position                           
     */
    public int getOvalY ()
    {
        return ovalY;
    }

    /**
     * 
     * @param ovalY y position
     */
    public void setOvalY (int ovalY)
    {
        this.ovalY = ovalY;        
    }

    /**
     * 
     * @return width
     */
    public int getOvalW ()
    {
        return ovalW;
    }

 
    /**
     * 
     * @return height
     */
    public int getOvalH ()
    {
        return ovalH;
    }



    @Override
    public boolean isSelected (int x, int y)
    {
        double rx = ovalW / 2.0;   // horizontal radius of ellipse
        double ry = ovalH / 2.0;  // vertical radius of ellipse 
        double cx = ovalX + rx;   // x-coord of center of ellipse
        double cy = ovalY + ry;    // y-coord of center of ellipse
       return (ry * (x - cx)) * (ry * (x - cx)) + (rx * (y - cy)) * (rx * (y - cy)) <= rx * rx * ry * ry;
    }

    @Override
    public void move (int x, int y)
    {
        ovalX += x;
        ovalY += y;
        
        
        setCoordinates(getOvalX(), getOvalY());

    }

    @Override
    public void draw (Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(currentColor);
        g2.fillOval(ovalX, ovalY, ovalW, ovalH);
        g2.setColor(Color.black);
        g2.drawOval(ovalX, ovalY, ovalW, ovalH);
    }

    @Override
    public void setcolor (Color color)
    {
        currentColor = color;
    }


}
