/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

/**
 *
 * @author mota
 */

/**
 * Oval class for JSON (de-)serialization.
 */
public class JsonOval extends JsonShape
{
   private int ovalX = 0;
   private int ovalY = 0;
   private final int ovalW = 100;
   private final int ovalH = 150;


   /**
    * 
    * @return the position of x 
    */
    public int getOvalX ()
    {
        return ovalX;
    }

    /**
     * 
     * @param ovalX represents the position of x 
     */
    public void setOvalX (int ovalX)
    {
        this.ovalX = ovalX;
    }

    /**
     * 
     * @return the position of y
     */
    public int getOvalY ()
    {
        return ovalY;
    }
    
    /**
     * 
     * @param ovalY represents the position of y
     */
    public void setOvalY (int ovalY)
    {
        this.ovalY = ovalY;
    }

    /**
     * 
     * @return the width of oval
     */
    public int getOvalW ()
    {
        return ovalW;
    }

    /**
     *
     * @return the height of oval
     */
    public int getOvalH ()
    {
        return ovalH;
    }
    
}
