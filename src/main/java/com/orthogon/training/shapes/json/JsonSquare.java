/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

/**
 *
 * @author mota
 */

/**
 * Square class for JSON (de-)serialization.
 */
public class JsonSquare extends JsonShape
{
    private int squareX = 0;
    private int squareY = 0;
    private final int squareW = 100;
    private final int squareH = 100;

    /**
     *
     * @return the position of x
     */
    public int getSquareX ()
    {
        return squareX;
    }

    /**
     *
     * @param squareX represents the position of x
     */
    public void setSquareX (int squareX)
    {
        this.squareX = squareX;
    }

    /**
     *
     * @return the position of y
     */
    public int getSquareY ()
    {
        return squareY;
    }

    /**
     *
     * @param squareY represents the position of y
     */
    public void setSquareY (int squareY)
    {
        this.squareY = squareY;
    }

    /**
     *
     * @return the width of Square
     */
    public int getSquareW ()
    {
        return squareW;
    }

    /**
     *
     * @return the height of Square
     */
    public int getSquareH ()
    {
        return squareH;
    }

}
