/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.fasterxml.jackson.databind.json.JsonMapper;
import com.fasterxml.jackson.databind.jsontype.BasicPolymorphicTypeValidator;
import com.fasterxml.jackson.databind.jsontype.PolymorphicTypeValidator;
import com.fasterxml.jackson.module.paramnames.ParameterNamesModule;
import com.orthogon.training.shapes.IPersistence;
import com.orthogon.training.shapes.Layer;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mota
 */
public class JsonPersistenceWithoutAnnotation implements IPersistence
{   
    private PolymorphicTypeValidator ptv = BasicPolymorphicTypeValidator
        .builder()
        .build();

    private ObjectMapper mapper = JsonMapper.builder()
        .activateDefaultTypingAsProperty(ptv, ObjectMapper.DefaultTyping.NON_FINAL, "type")
        .enable(SerializationFeature.INDENT_OUTPUT)
        .build();
    
    
    
    @Override
    public String getExtension ()
    {
        return "Json without Annotations";
    }

    @Override
    public void save (List<Layer> layers, File file)
    {   
      
        mapper.registerModule(new ParameterNamesModule());
        try
        {
            mapper.writerWithDefaultPrettyPrinter().writeValue(file, layers);           
        }
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(JSONPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex)
        {
            LOG.log(Level.WARNING, "Failed to read from json file <{0}>.", file.getName());
        }
    }

    @Override
    public List<Layer> load (File file)
    {   
                
        List<Layer> jsonLayers = new ArrayList<>();
        try
        {

            TypeReference<List<Layer>> mapType;
            mapType = new TypeReference<List<Layer>>()
            {

            };

            jsonLayers = mapper.readValue(file, mapType);
        }

        catch (IOException ex)
        {
            LOG.log(Level.WARNING, "Failed to read from json file <{0}>.", file.getName());
        }
        
        return jsonLayers;
    }
    
    

}
