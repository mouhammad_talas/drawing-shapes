/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

/**
 *
 * @author mota
 */

/**
 * Rectangle class for JSON (de-)serialization.
 */
public class JsonRectangle extends JsonShape
{
   private int rectangleX = 0;
   private int rectangleY = 0;
   private final int rectangleW = 200;
   private final int rectangleH = 100;


   /**
    * 
    * @return the position of x 
    */
    public int getRectangleX ()
    {
        return rectangleX;
    }

    /**
     * 
     * @param rectangleX represents the position of x 
     */
    public void setRectangleX (int rectangleX)
    {
        this.rectangleX = rectangleX;
    }

    /**
     * 
     * @return the position of y
     */
    public int getRectangleY ()
    {
        return rectangleY;
    }
    
    /**
     * 
     * @param rectangleY represents the position of y
     */
    public void setRectangleY (int rectangleY)
    {
        this.rectangleY = rectangleY;
    }

    /**
     * 
     * @return the width of Rectangle
     */
    public int getRectangleW ()
    {
        return rectangleW;
    }

    /**
     *
     * @return the height of Rectangle
     */
    public int getRectangleH ()
    {
        return rectangleH;
    }

    
}
