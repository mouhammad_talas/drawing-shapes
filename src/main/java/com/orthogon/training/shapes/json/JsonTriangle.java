/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

import com.orthogon.training.shapes.Triangle;

/**
 *
 * @author mota
 */

/**
 * Triangle class for JSON (de-)serialization.
 */
public class JsonTriangle extends JsonShape
{
    private final Triangle triangle = new Triangle();
    private int[] triangleX = new int[triangle.getTriangleX().length];
    private int[] triangleY = new int[triangle.getTriangleY().length];
    
    /**
    * 
    * @return the position of x 
    */
    public int[] getTriangleX ()
    {
        return triangleX;
    }
    
    /**
     * 
     * @param triangleX represents the positions of x 
     */
    public void setTriangleX (int[] triangleX)
    {
        this.triangleX = triangleX;
    }

    /**
     * 
     * @return the position of y
     */
    public int[] getTriangleY ()
    {
        return triangleY;
    }

    /**
     * 
     * @param triangleY represents the positions of y
     */
    public void setTriangleY (int[] triangleY)
    {
        this.triangleY = triangleY;
    }
    
    
}
