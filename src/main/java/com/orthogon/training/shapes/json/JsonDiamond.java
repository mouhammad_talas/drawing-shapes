/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

import com.orthogon.training.shapes.Diamond;

/**
 *
 * @author mota
 */


/**
 * Diamond class for JSON (de-)serialization.
 */
public class JsonDiamond extends JsonShape
{
    private final Diamond diamond = new Diamond();
    private int[] diamondX = new int[diamond.getDiamondX().length];
    private int[] diamondY = new int[diamond.getDiamondY().length];
    
    /**
    * 
    * @return the position of x 
    */
    public int[] getDiamondX ()
    {
        return diamondX;
    }
    
    /**
     * 
     * @param diamondX represents the positions of x 
     */
    public void setDiamondX (int[] diamondX)
    {
        this.diamondX = diamondX;
    }

    /**
     * 
     * @return the position of y
     */
    public int[] getDiamondY ()
    {
        return diamondY;
    }

    /**
     * 
     * @param diamondY represents the positions of y
     */
    public void setDiamondY (int[] diamondY)
    {
        this.diamondY = diamondY;
    }
    
    
}
