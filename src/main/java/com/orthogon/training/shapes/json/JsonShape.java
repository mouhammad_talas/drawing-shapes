/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonTypeInfo.Id;

/**
 *
 * @author mota
 */


@JsonTypeInfo(use = Id.CLASS,
              include = JsonTypeInfo.As.PROPERTY,
              property = "type")
@JsonSubTypes({
    @Type(value = JsonCircle.class),
    @Type(value = JsonTriangle.class),
    @Type(value = JsonDiamond.class),
    @Type(value = JsonOval.class),
    @Type(value = JsonRectangle.class),
    @Type(value = JsonSquare.class),
})

/**
 * Shape abstract class for JSON (de-)serialization.
 */
public abstract class JsonShape
{
    
    
}

