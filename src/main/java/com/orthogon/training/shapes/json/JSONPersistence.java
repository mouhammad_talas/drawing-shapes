/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.orthogon.training.shapes.Circle;
import com.orthogon.training.shapes.Diamond;
import com.orthogon.training.shapes.IPersistence;
import com.orthogon.training.shapes.Layer;
import com.orthogon.training.shapes.Oval;
import com.orthogon.training.shapes.Rectangle;
import com.orthogon.training.shapes.Square;
import com.orthogon.training.shapes.Triangle;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mota
 */
public class JSONPersistence implements IPersistence
{

    private final ObjectMapper objectMapper = new ObjectMapper();
    private List<JsonLayer> jsonLayers = new ArrayList<>();

    @Override
    public String getExtension ()
    {
        return "json";
    }

    @Override
    public void save (List<Layer> layers, File file)
    {

        objectMapper.enable(SerializationFeature.INDENT_OUTPUT);

        for (int i = 0; i < 10; i++)
        {
            jsonLayers.add(new JsonLayer());
        }

        layers.stream().forEach((layer) ->
        {
                
            
            
            layer.getElements().forEach((key, shape) ->
            {

                if (shape instanceof Circle)
                {
                    Circle circle = (Circle) shape;
                    JsonCircle jsonCircle = new JsonCircle();

                    jsonCircle.setCircleX(circle.getCircleX());
                    jsonCircle.setCircleY(circle.getCircleY());

                    jsonLayers.get(layers.indexOf(layer)).getJsonElements().add(jsonCircle);

                }
                else if (shape instanceof Triangle)
                {
                    Triangle triangle = (Triangle) shape;
                    JsonTriangle jsonTriangle = new JsonTriangle();

                    jsonTriangle.setTriangleX(triangle.getTriangleX());
                    jsonTriangle.setTriangleY(triangle.getTriangleY());

                    jsonLayers.get(layers.indexOf(layer)).getJsonElements().add(jsonTriangle);
                }
                else if (shape instanceof Diamond)
                {
                    Diamond diamond = (Diamond) shape;
                    JsonDiamond jsonDiamond = new JsonDiamond();

                    jsonDiamond.setDiamondX(diamond.getDiamondX());
                    jsonDiamond.setDiamondY(diamond.getDiamondY());

                    jsonLayers.get(layers.indexOf(layer)).getJsonElements().add(jsonDiamond);
                }
                else if (shape instanceof Oval)
                {
                    Oval oval = (Oval) shape;
                    JsonOval jsonOval = new JsonOval();

                    jsonOval.setOvalX(oval.getOvalX());
                    jsonOval.setOvalY(oval.getOvalY());

                    jsonLayers.get(layers.indexOf(layer)).getJsonElements().add(jsonOval);
                }
                else if (shape instanceof Square)
                {
                    Square square = (Square) shape;
                    JsonSquare jsonSquare = new JsonSquare();

                    jsonSquare.setSquareX(square.getSquareX());
                    jsonSquare.setSquareY(square.getSquareY());

                    jsonLayers.get(layers.indexOf(layer)).getJsonElements().add(jsonSquare);
                }
                else if (shape instanceof Rectangle)
                {
                    Rectangle rectangle = (Rectangle) shape;
                    JsonRectangle jsonRectangle = new JsonRectangle();

                    jsonRectangle.setRectangleX(rectangle.getRectangleX());
                    jsonRectangle.setRectangleY(rectangle.getRectangleY());

                    jsonLayers.get(layers.indexOf(layer)).getJsonElements().add(jsonRectangle);
                }

            });

        });

        try
        {
            objectMapper.writeValue(file, jsonLayers);
        }
        catch (JsonProcessingException ex)
        {
            Logger.getLogger(JSONPersistence.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IOException ex)
        {
            LOG.log(Level.WARNING, "Failed to read from json file <{0}>.", file.getName());
        }

    }

    @Override
    public List<Layer> load (File file)
    {

        List<Layer> layers = new ArrayList<>();

        for (int i = 0; i < 10; i++)
        {
            layers.add(new Layer());
        }

        try
        {
            TypeReference<List<JsonLayer>> mapType;
            mapType = new TypeReference<List<JsonLayer>>()
            {

            };

            jsonLayers = objectMapper.readValue(file, mapType);
        }

        catch (IOException ex)
        {
            LOG.log(Level.WARNING, "Failed to read from json file <{0}>.", file.getName());
        }

        jsonLayers.stream().forEach((jLayer) ->
        {

            jLayer.getJsonElements().stream().forEach((jsonShape) ->
            {

                if (jsonShape instanceof JsonCircle)
                {

                    JsonCircle jsonCircle = (JsonCircle) jsonShape;

                    Circle circle = new Circle();

                    circle.setCircleX(jsonCircle.getCircleX());
                    circle.setCircleY(jsonCircle.getCircleY());

                    layers.get(jsonLayers.indexOf(jLayer)).add(circle);
                }
                else if (jsonShape instanceof JsonTriangle)
                {
                    JsonTriangle jsonTriangle = (JsonTriangle) jsonShape;
                    Triangle triangle = new Triangle();

                    triangle.setTriangleX(jsonTriangle.getTriangleX());
                    triangle.setTriangleY(jsonTriangle.getTriangleY());

                    layers.get(jsonLayers.indexOf(jLayer)).add(triangle);
                }

                else if (jsonShape instanceof JsonDiamond)
                {
                    JsonDiamond jsonDiamond = (JsonDiamond) jsonShape;
                    Diamond diamond = new Diamond();

                    diamond.setDiamondX(jsonDiamond.getDiamondX());
                    diamond.setDiamondY(jsonDiamond.getDiamondY());

                    layers.get(jsonLayers.indexOf(jLayer)).add(diamond);
                }
                else if (jsonShape instanceof JsonOval)
                {
                    JsonOval jsonOval = (JsonOval) jsonShape;
                    Oval oval = new Oval();

                    oval.setOvalX(jsonOval.getOvalX());
                    oval.setOvalY(jsonOval.getOvalY());

                    layers.get(jsonLayers.indexOf(jLayer)).add(oval);
                }
                else if (jsonShape instanceof JsonSquare)
                {
                    JsonSquare jsonSquare = (JsonSquare) jsonShape;
                    Square square = new Square();

                    square.setSquareX(jsonSquare.getSquareX());
                    square.setSquareY(jsonSquare.getSquareY());

                    layers.get(jsonLayers.indexOf(jLayer)).add(square);
                }
                else if (jsonShape instanceof JsonRectangle)
                {
                    JsonRectangle jsonRectangle = (JsonRectangle) jsonShape;
                    Rectangle rectangle = new Rectangle();

                    rectangle.setRectangleX(jsonRectangle.getRectangleX());
                    rectangle.setRectangleY(jsonRectangle.getRectangleY());

                    layers.get(jsonLayers.indexOf(jLayer)).add(rectangle);
                }
            });

        });

        return layers;
    }

}
