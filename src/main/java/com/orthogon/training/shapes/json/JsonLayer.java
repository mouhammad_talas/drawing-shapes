/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author mota
 */

/**
 * 
 * Layer class for JSON (de-)serialization.
 */
public class JsonLayer
{
    private List<JsonShape> jsonElements = new ArrayList<>();

    /**
     * 
     * @return a list of shapes. 
     */
    public List<JsonShape> getJsonElements ()
    {
        return jsonElements;
    }

    /**
     * 
     * @param jsonElements represents a list of shapes. 
     */
    public void setJsonElements (List<JsonShape> jsonElements)
    {
        this.jsonElements = jsonElements;
    }
    
}
