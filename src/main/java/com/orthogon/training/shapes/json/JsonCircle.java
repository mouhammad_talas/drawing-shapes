/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes.json;

/**
 *
 * @author mota
 */

/**
 * Circle class for JSON (de-)serialization.
 */
public class JsonCircle extends JsonShape

{
    private int circleX = 0;
    private int circleY = 0;
    private final int circleW = 100;
    private final int circleH = 100;

    /**
     *
     * @return the position of x
     */
    public int getCircleX ()
    {
        return circleX;
    }

    /**
     *
     * @param circleX represents the position of x
     */
    public void setCircleX (int circleX)
    {
        this.circleX = circleX;
    }

    /**
     *
     * @return the position of y
     */
    public int getCircleY ()
    {
        return circleY;
    }

    /**
     *
     * @param circleY represents the position of y
     */
    public void setCircleY (int circleY)
    {
        this.circleY = circleY;
    }

    /**
     *
     * @return the width of circle
     */
    public int getCircleW ()
    {
        return circleW;
    }

    /**
     *
     * @return the height of circle
     */
    public int getCircleH ()
    {
        return circleH;
    }

}
