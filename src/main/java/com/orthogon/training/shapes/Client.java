/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.SocketChannel;
import java.util.Iterator;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author mota
 */
public class Client implements Runnable
{
    private SocketChannel channel;
    private Thread thread;
    private final Consumer<InputStream> reader;

    private static final int PORT = 56255;

    /**
     *
     * @param reader consumer
     */
    public Client (Consumer<InputStream> reader)
    {
        this.reader = reader;
    }

    /**
     * giving order to start the thread and open socket channel with the server.
     */
    public void start ()
    {
        try
        {
            channel = SocketChannel.open(new InetSocketAddress("localhost", PORT));

        }
        catch (IOException ex)
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
        thread = new Thread(this);
        thread.start();
    }

    /**
     * to close the contact with the server and terminate the thread.
     */
    public void stop ()
    {
        try
        {
            channel.close();
            thread.interrupt();
            thread.join();
        }
        catch (IOException ex)
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);

        }
        catch (InterruptedException ex)
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void run ()
    {

        Selector selector;
        try
        {
            selector = Selector.open();
            channel.configureBlocking(false);
            channel.register(selector, SelectionKey.OP_READ);

            while (true)
            {
                selector.select();
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> iter = selectedKeys.iterator();
                while (iter.hasNext())
                {
                    SelectionKey key = iter.next();

                    if (key.isReadable())
                    {
                        ByteBuffer header = ByteBuffer.allocate(Integer.BYTES);

                        do
                        {
                            channel.read(header);
                        }
                        while (header.remaining() > 0);

                        header.rewind();
                        int length = header.getInt();
                        ByteBuffer buffer = ByteBuffer.allocate(length);

                        do
                        {
                            channel.read(buffer);
                        }
                        while (buffer.remaining() > 0);

                        buffer.rewind();
                        InputStream is = new ByteArrayInputStream(buffer.array());
                        reader.accept(is);
                    }

                    iter.remove();
                }
                if (Thread.currentThread().isInterrupted())
                {
                    break;
                }
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * sending the data as object over the network to the server side.
     *
     * @param object
     */
    void send (Object object)
    {
        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ObjectOutputStream oos;
        try
        {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);

            ByteBuffer buffer = ByteBuffer.wrap(baos.toByteArray());

            ByteBuffer header = ByteBuffer.allocate(Integer.BYTES);  // this allocate methode takes capacity in bytes as parameter, in this one it is 4 bytes. and it returns a byte Buffer.
            header.putInt(buffer.capacity()); // can we put capacity method instead of remaining !!////// writes 4 bytes containing the int (buffer Capacity) value, and the position increments by four
            header.rewind(); //This method sets the position to 0 because it is now by 4 and the remaining will be equated to the capacity.

            do
            {
                channel.write(header); // 
            }
            while (header.hasRemaining()); // hasRemaining method works too  // to write in the header completly 
            do
            {
                channel.write(buffer);
            }
            while (buffer.hasRemaining());

            header.rewind();
            buffer.rewind();
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

}
