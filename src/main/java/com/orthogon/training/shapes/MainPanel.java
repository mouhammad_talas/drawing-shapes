package com.orthogon.training.shapes;

import com.orthogon.training.shapes.json.JSONPersistence;
import com.orthogon.training.shapes.protoBuff.ProtoBuffPersistence;
import com.orthogon.training.shapes.binary.BinaryPersistence;
import com.orthogon.training.shapes.json.JsonPersistenceWithoutAnnotation;
import com.orthogon.training.shapes.xml.XMLPersistence;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Point;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseMotionAdapter;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFileChooser;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JSpinner;
import javax.swing.JToggleButton;
import javax.swing.SpinnerNumberModel;
import javax.swing.filechooser.FileNameExtensionFilter;

/**
 * @author Mota
 */
public class MainPanel extends JPanel
{
    private Point startPoint = new Point();
    private Point offset = new Point();
    private final int widthDimension = 850;
    private final int heightDimension = 610;

    private XMLPersistence xmlPers = new XMLPersistence();
    private BinaryPersistence binaryPers = new BinaryPersistence();
    private ProtoBuffPersistence pBuffPers = new ProtoBuffPersistence();
    private JSONPersistence jsonPers = new JSONPersistence();
    private JsonPersistenceWithoutAnnotation withoutAnnotation = new JsonPersistenceWithoutAnnotation();

    private JFileChooser chooser = new JFileChooser("/export/home/mota/Downloads"); // src/Files represinting the current directury path.
    private final FileNameExtensionFilter xmlFilter = new FileNameExtensionFilter(xmlPers.getExtension(), xmlPers.getExtension());
    private final FileNameExtensionFilter serFilter = new FileNameExtensionFilter(binaryPers.getExtension(), binaryPers.getExtension());
    private final FileNameExtensionFilter pBufFilter = new FileNameExtensionFilter(pBuffPers.getExtension(), pBuffPers.getExtension());
    private final FileNameExtensionFilter jsonFilter = new FileNameExtensionFilter(jsonPers.getExtension(), jsonPers.getExtension());
    private final FileNameExtensionFilter jsonFilterWithOutAnnotation = new FileNameExtensionFilter(withoutAnnotation.getExtension(), "json");

    private final JLabel setLayer = new JLabel("Layer Nr:");
    private final SpinnerNumberModel model = new SpinnerNumberModel(1, 1, 10, 1);
    private JSpinner layerSpinner = new JSpinner(model);
    private final JToggleButton connectAsServer = new JToggleButton("Connect as Server");
    private final JToggleButton connectAsClient = new JToggleButton("Connect as Client");
    private final JToggleButton optimizing = new JToggleButton("With Optimizing");
    private JLabel connectionStatus = new JLabel();
//    private int index = 0;
    private static final int NUMBER_OF_LAYERS = 10;
    private List<Layer> layers;
    private Client client = null;
    private Shape selectedShape = null;
    private Shape tempSelected = null;
    private IPersistence persistence = null;
    private Server server = null;
    private MovementsMessage movementsMessage;
    private CreateMessage createMessage;
    private DeleteMessage deleteMessage;
    private boolean optimize = false;
    private HashMap<UUID, Shape> shapeMap;

    /**
     *
     * @param filter file extension.
     * @return instance of the wanted persistence class.
     */
    IPersistence getPersistenceInstance (FileNameExtensionFilter filter)
    {
        if (filter == xmlFilter)
        {
            return new XMLPersistence();
        }
        else if (filter == serFilter)
        {
            return new BinaryPersistence();
        }
        else if (filter == jsonFilterWithOutAnnotation)
        {
            return new JsonPersistenceWithoutAnnotation();
        }
        else if (filter == jsonFilter)
        {
            return new JSONPersistence();
        }
        else if (filter == pBufFilter)
        {
            return new ProtoBuffPersistence();
        }

        return null;
    }

    /**
     * the MainPanel constructor that include all actions and events from
     * buttons and mouse clicks.
     */
    public MainPanel ()
    {
        layers = new ArrayList<>();
        shapeMap = new HashMap<>();
        chooser.addChoosableFileFilter(xmlFilter);  // with File Filte, just the files type that i want can i choose.
        chooser.addChoosableFileFilter(serFilter);
        chooser.addChoosableFileFilter(pBufFilter);
        chooser.addChoosableFileFilter(jsonFilter);
        chooser.addChoosableFileFilter(jsonFilterWithOutAnnotation);
        add(connectAsServer);
        add(connectAsClient);
        add(optimizing);

        for (int i = 0; i < NUMBER_OF_LAYERS; i++)
        {
            layers.add(new Layer());
        }

        add(setLayer);
        add(layerSpinner);

        setBorder(BorderFactory.createLineBorder(Color.gray));

        addButton(this, "add Circle", (ActionEvent e) ->
              {
                  addShape(new Circle());
              });

        addButton(this, "add Square", (ActionEvent e) ->
              {
                  addShape(new Square());
              });

        addButton(this, "add Oval", (ActionEvent e) ->
              {
                  addShape(new Oval());
              });

        addButton(this, "add Diamond", (ActionEvent e) ->
              {
                  addShape(new Diamond());
              });

        addButton(this, "add Rectangle", (ActionEvent e) ->
              {
                  addShape(new Rectangle());
              });

        addButton(this, "add Triangle", (ActionEvent e) ->
              {
                  addShape(new Triangle());
              });

        addButton(this, "Save", (ActionEvent e) ->
              {
                  chooser.showSaveDialog(null);
                  File selectedFile = chooser.getSelectedFile();

                  persistence = getPersistenceInstance((FileNameExtensionFilter) chooser.getFileFilter());

                  persistence.save(layers, selectedFile);
              });

        addButton(this, "Load", (ActionEvent e) ->
              {
                  // with null will the dialog appearing in the center, or i can choose the frame that i want to be showed in.
                  chooser.showOpenDialog(null);
                  File selectedFile = chooser.getSelectedFile();

                  persistence = getPersistenceInstance((FileNameExtensionFilter) chooser.getFileFilter());

                  List<Layer> loadedLayers = persistence.load(selectedFile);
                  layers = loadedLayers;
                  repaint();
              });

        addButton(this, "delete All", (ActionEvent e) ->
              {
                  for (int i = 0; i < layers.size(); i++)
                  {
                      layers.get(i).getElements().clear();
                  }
                  refresh();
                  repaint();
              });
        
        // clicking the Connect as server will call this method, that establish a contact with other clients. 
        connectAsServer.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed (ActionEvent e)
            {
                if (connectAsServer.isSelected())
                {
                    server = new Server((SocketChannel client) ->
                    {
                        server.send(layers, client);

                    }, (InputStream is) ->
                                    {
                                        load(is);
                                        refresh();
                                    });

                    server.start();

                }
                else
                {
                    server.stop();

                }
            }
        });

        connectAsClient.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed (ActionEvent e)
            {
                if (connectAsClient.isSelected())
                {
                    client = new Client((InputStream is) ->
                    {
                        load(is);
                    });
                    client.start();
                }
                else
                {

                    client.stop();
                }
            }
        });

        optimizing.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed (ActionEvent e)
            {
                if (optimizing.isSelected())
                {
                    optimize = true;
                }
            }
        });

        addMouseListener(
            new MouseAdapter()
        {

            @Override
            public void mousePressed (MouseEvent e
            )
            {
                startPoint = new Point(e.getX(), e.getY());

                for (int i = layers.size() - 1; i >= 0 && selectedShape == null; i--)
                {
                    selectedShape = layers.get(i).select(e.getX(), e.getY());
                    tempSelected = selectedShape;
                }

            }

            @Override
            public void mouseReleased (MouseEvent e
            )
            {
                selectedShape = null;
            }

            @Override
            public void mouseClicked (MouseEvent e
            )
            {
                if (e.getClickCount() == 2)
                {
                    layers.get(((Integer) layerSpinner.getValue()) - 1).getElements().remove(tempSelected.getId());
                    if (optimize)
                    {
                        refrechDeletes(tempSelected);
                    }
                    else
                    {
                        refresh();
                    }
                    repaint();
                }
            }
        }
        );

        addMouseMotionListener(new MouseMotionAdapter()
        {
            @Override
            public void mouseDragged (MouseEvent e
            )
            {
                offset = new Point(e.getX() - ((int) startPoint.getX()), e.getY() - ((int) startPoint.getY()));
                startPoint = new Point(e.getX(), e.getY());

                if (selectedShape != null)
                {
                    selectedShape.move((int) offset.getX(), (int) offset.getY());

                    if (optimize)
                    {
                        refreshMovements(selectedShape); // that means every time a shape is moved with dragging the mouse, a movment message with new
                                                        // coordinates will be sent to the other side.

                    }
                    else
                    {
                        refresh();                      // a set of layer that contains shapes will be sent, every time a shape is moved 
                    }

                    repaint();
                }
            }
        }
        );
//        addMouseWheelListener((MouseWheelEvent e) ->
//        {
//            if (selectedShape != null && e.getWheelRotation() < 0)
//            {
//                index = layers.get(((Integer) layerSpinner.getValue()) - 1).getElements().indexOf(selectedShape); // the index of my selected shape will be saved in the variable index
//
//                Collections.swap(layers.get(((Integer) layerSpinner.getValue()) - 1).getElements(), validate(index), validate(index + 1));  // my selected shape will be swaped with the shape after
//
//                index += 1;  // here will be the new index of my selected shape saved
//            }
//
//            else if (selectedShape != null && e.getWheelRotation() > 0)
//            {
//                index = layers.get(((Integer) layerSpinner.getValue()) - 1).getElements().indexOf(selectedShape);
//
//                Collections.swap(layers.get(((Integer) layerSpinner.getValue()) - 1).getElements(), validate(index), validate(index - 1));
//
//                index -= 1;
//            }
//            refresh();
//            repaint();
//        });       
    }

    /**
     * Adds the given shape to the currently selected layer.
     *
     * @param shape the shape to add.
     */
    private void addShape (Shape shape)
    {
        int index = ((Integer) layerSpinner.getValue()) - 1;
        layers.get(index).getElements().put(shape.getId(), shape);
//        System.out.println(shape.getId());

        if (optimize)
        {
            refrechCreates(shape, index);
        }
        else
        {
            refresh();
        }
        repaint();
    }

    /**
     * it sends the set of layers over the
     * network to the client or server.
     */
    private void refresh ()
    {
        if (server != null)
        {
            server.send(layers);
        }
        if (client != null)
        {
            client.send(layers);
        }
    }

    /**
     * it sends coordinates and id of shape over the network.
     * @param shape Shape
     */
    private void refreshMovements (Shape shape)
    {
        movementsMessage = new MovementsMessage(shape.getId(), shape.getCoordinates().x, shape.getCoordinates().y, offset);

        if (server != null)
        {
            server.send(movementsMessage);
        }
        if (client != null)
        {
            client.send(movementsMessage);
        }
    }
    
    /**
     * it sends a message contains id and the shape type over the network.
     * @param shape Shape
     * @param index index of layer
     */
    private void refrechCreates (Shape shape, int index)
    {
        createMessage = new CreateMessage(shape.getId(), (Class<Shape>) shape.getClass(), index);

        if (server != null)
        {
            server.send(createMessage);

        }
        if (client != null)
        {
            client.send(createMessage);
        }
    }

    /**
     * sending the id of shape, that should be deleted.
     * @param shape Shape
     */
    private void refrechDeletes (Shape shape)
    {
        deleteMessage = new DeleteMessage(shape.getId());

        if (server != null)
        {
            server.send(deleteMessage);
        }
        if (client != null)
        {
            client.send(deleteMessage);
        }
    }


    /**
     * 
     * @param layers 
     */
    private void refreshShapeMap (List<Layer> layers)
    {
        shapeMap.clear();
        layers.forEach(layer ->
        {
            layer.getElements().forEach((key, shape) ->
            {
                shapeMap.put(key, shape);
            });
        });
    }

    /**
     * loading the data that been sent either from client or from server.
     * @param is Stream of input
     */
    private void load (InputStream is)
    {
        ObjectInputStream ois;
        try
        {
            ois = new ObjectInputStream(is);

            UUID id;
            int x;
            int y;
            Point offsetTemp;

            Object result = ois.readObject();

            if (result instanceof CreateMessage) // to figure out if the data that come either from client or server ist instance of Createmessage.
            {
                if (optimize)
                {

                    CreateMessage createMessageNew = (CreateMessage) result;
                    createMessage = createMessageNew;

                    Constructor<? extends Shape> constructer = createMessage.getShapeClass().getConstructor(UUID.class);

                    layers.get(createMessage.getLayerIndex()).add(constructer.newInstance(createMessage.getId()));
                    refreshShapeMap(layers);

                    System.out.println("CreateMessage id: " + createMessage.getId());
                }

            }
            else if (result instanceof MovementsMessage)
            {
                MovementsMessage movementsMessageNew = (MovementsMessage) result;
                movementsMessage = movementsMessageNew;

                if (movementsMessage.getId() != null && optimize)
                {
                    id = movementsMessage.getId();
                    x = movementsMessage.getX();
                    y = movementsMessage.getY();
                    offsetTemp = movementsMessage.getOffset();

                    Shape shape = (Shape) shapeMap.get(id);

                    shape.move((int) offsetTemp.getX(), (int) offsetTemp.getY());
                    System.out.println("X-Offset : " + (int) offsetTemp.getX() + "Y-Offset : " + (int) offsetTemp.getY());

                }

            }

            else if (result instanceof DeleteMessage)
            {
                DeleteMessage deleteMessageNew = (DeleteMessage) result;
                deleteMessage = deleteMessageNew;

                if (optimize)
                {
                    Shape shape = (Shape) shapeMap.get(deleteMessage.getId());

                    for (Layer layer : layers)
                    {
                        layer.getElements().remove(deleteMessage.getId());
                    }

                    refreshShapeMap(layers);
                }
            }

            else
            {
                List<Layer> aLayers = (List<Layer>) result;
                layers = aLayers;
                refreshShapeMap(layers);

            }

            repaint();
        }
        catch (IOException | ClassNotFoundException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InstantiationException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IllegalAccessException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (NoSuchMethodException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (SecurityException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (IllegalArgumentException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }
        catch (InvocationTargetException ex)
        {
            Logger.getLogger(MainPanel.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    @Override
    public Dimension getPreferredSize ()
    {
        return new Dimension(widthDimension, heightDimension);
    }

    @Override
    protected void paintComponent (Graphics g)
    {
        super.paintComponent(g);

        int layerIndex = 0;

        layers.get(layerIndex++).setColor(Color.yellow);
        layers.get(layerIndex++).setColor(Color.blue);
        layers.get(layerIndex++).setColor(Color.green);
        layers.get(layerIndex++).setColor(Color.orange);
        layers.get(layerIndex++).setColor(Color.red);
        layers.get(layerIndex++).setColor(Color.lightGray);
        layers.get(layerIndex++).setColor(Color.magenta);
        layers.get(layerIndex++).setColor(Color.pink);
        layers.get(layerIndex++).setColor(Color.cyan);
        layers.get(layerIndex++).setColor(Color.darkGray);
        layers.forEach((layer) ->
        {
            layer.draw(g);
        });
    }

    /**
     * this method returns the value (index) so that it's not bigger than the
     * size of shape list. and not smaller than null, this method i can use it
     * to swap the objects in my shape list throw the index
     *
     * @param value represents the index of a list.
     * @return gets a value that not bigger or smaller than the list size.
     */
    public int validate (int value)
    {

        if (value > layers.get(((Integer) layerSpinner.getValue()) - 1).getElements().size() - 1)
        {
            return layers.get(((Integer) layerSpinner.getValue()) - 1).getElements().size() - 1;
        }
        else if (value < 0)
        {
            return 0;
        }

        else
        {
            return value;
        }
    }

    /**
     *
     * @param c the Container
     * @param title the name of the Button
     * @param listener the action listener
     */
    public void addButton (Container c, String title, ActionListener listener)
    {
        JButton button = new JButton(title);
        c.add(button);
        button.addActionListener(listener);
    }
}
