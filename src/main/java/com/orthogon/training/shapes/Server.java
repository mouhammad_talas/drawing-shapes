/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectOutputStream;
import java.net.InetSocketAddress;
import java.nio.Buffer;
import java.nio.ByteBuffer;
import java.nio.channels.SelectionKey;
import java.nio.channels.Selector;
import java.nio.channels.ServerSocketChannel;
import java.nio.channels.SocketChannel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.function.Consumer;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * the purpose of Server class is to communicate with the client class and
 * establish a contact.
 *
 * @author mota
 */
public class Server implements Runnable
{

    private ServerSocketChannel channel;
    private static final int PORT = 56255;
    private Thread thread;
    private List<SocketChannel> clients = new ArrayList<>();
    private Queue<SocketChannel> clientsQueue = new LinkedList();
    private final Consumer<SocketChannel> clientConnected;
    private final Consumer<InputStream> reader;

    /**
     * Server Constructor.
     *
     * @param clientConnected
     * @param reader
     */
    public Server (Consumer<SocketChannel> clientConnected, Consumer<InputStream> reader)
    {
        this.clientConnected = clientConnected;
        this.reader = reader;
    }

    /**
     * giving order to start the thread and open socket channel with the server.
     */
    public void start ()
    {
        try
        {
            channel = ServerSocketChannel.open();
            channel.bind(new InetSocketAddress("localhost", PORT));
            System.out.println("ready to connecting .......\n"
                               + "waiting for Client .........");
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
        thread = new Thread(this);
        thread.start();
    }

    /**
     * to close the contact with the server and terminate the thread.
     */
    public void stop ()
    {
        try
        {
            channel.close();
            thread.interrupt();
            thread.join();
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);

        }
        catch (InterruptedException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
            Thread.currentThread().interrupt();
        }
    }

    @Override
    public void run ()
    {
        try
        {
            Selector selector = Selector.open();
            channel.configureBlocking(false);
            channel.register(selector, SelectionKey.OP_ACCEPT);

            while (true)
            {
                selector.select();
                Set<SelectionKey> selectedKeys = selector.selectedKeys();
                Iterator<SelectionKey> iter = selectedKeys.iterator();

                while (iter.hasNext())
                {

                    SelectionKey key = iter.next();

                    if (key.isAcceptable()) // this means that the registered Server channel is ready and can accept the connection  
                    {
                        register(selector, channel); // so here registering the client channel to be ready for reading operations  
                    }

                    if (key.isReadable())
                    {
                        ByteBuffer header = ByteBuffer.allocate(Integer.BYTES);

                        do
                        {
                            for (SocketChannel client : clients)
                            {
                                int byteSize = client.read(header);

                                if (byteSize > 0) // when the bytes are bigger than 0, that means that this client has data to send, and we have to determine this client.
                                {
                                    clientsQueue.add(client); // the client added at the top of Queue.
                                }
                            }

                        }
                        while (header.hasRemaining());

                        header.rewind();
                        int length = header.getInt();
                        ByteBuffer buffer = ByteBuffer.allocate(length);

                        do
                        {
                            for (SocketChannel client : clients)
                            {
                                client.read(buffer);
                            }
                        }
                        while (buffer.hasRemaining());

                        buffer.rewind();
                        InputStream is = new ByteArrayInputStream(buffer.array());
                        reader.accept(is);
                    }
                    iter.remove();
                }
            }
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     *
     * @param selector Selector
     * @param serverSocket Server Socket Channel
     * @throws IOException Input Output Exception
     */
    private void register (Selector selector, ServerSocketChannel serverSocket)
        throws IOException
    {
        SocketChannel client = serverSocket.accept(); // the server channel accepting the connect to client channel.
        System.out.println("the client is connected");
        client.configureBlocking(false); // putting the channel in non-blocking mode.
        client.register(selector, SelectionKey.OP_READ); // prepare to read from Server
        clients.add(client); // adding this client channel to the list, in case we have more clients, we can handling with them
        clientConnected.accept(client); // the consumer clientConnected accept the client channel as input to use it in the constructor of Server class.
    }

    /**
     *
     * @param object the object that should be sent
     * @param client the client that should receive the object.
     */
    public void send (Object object, SocketChannel client)
    {
        sendToClient(client, writingToBuffer(object));

    }

    /**
     *
     * @param object the object that should be sent
     */
    public void send (Object object)
    {

        for (SocketChannel client : clients)
        {
            if (client != clientsQueue.peek()) // peek() returns the first added element to the queue, without removing it.
            {
                sendToClient(client, writingToBuffer(object));
            }
        }

        clientsQueue.poll(); // why not using remove()!!, because remove() throws NoSuchElementExeption if the queue is empty, poll() returns null if the queue is empty.
        // now after this if the server want to send data, it can send to all clients
    }

    /**
     *
     * @param object the object will be written to buffer.
     * @return Byte Buffer
     */
    public ByteBuffer writingToBuffer (Object object)
    {

        ByteArrayOutputStream baos = new ByteArrayOutputStream();

        ObjectOutputStream oos;
        try
        {
            oos = new ObjectOutputStream(baos);
            oos.writeObject(object);
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }

        System.out.println(baos.size());

        return ByteBuffer.wrap(baos.toByteArray()); // this methode wrapes a byte array into byte Buffer.;

    }

    /**
     *
     * @param client the client that should receive the buffer.
     * @param buffer Byte Buffer
     */
    public void sendToClient (SocketChannel client, ByteBuffer buffer)
    {

        try
        {
            ByteBuffer header = ByteBuffer.allocate(Integer.BYTES);  // this allocate methode takes capacity in bytes as parameter, in this one it is 4 bytes. and it returns a byte Buffer.
            header.putInt(buffer.capacity()); // can we put capacity method instead of remaining !!////// writes 4 bytes containing the int (buffer Capacity) value, and the position increments by four
            ((Buffer) header).rewind(); //This method sets the position to 0 because it is now by 4 and the remaining will be equated to the capacity.

            do
            {
                client.write(header); // 
            }
            while (header.hasRemaining()); // hasRemaining method works too  // to write in the header completly 
            do
            {
                client.write(buffer);
            }
            while (buffer.hasRemaining());

            header.rewind();
            buffer.rewind();
        }
        catch (IOException ex)
        {
            Logger.getLogger(Server.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
