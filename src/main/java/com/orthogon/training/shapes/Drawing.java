/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JFrame;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

/**
 *
 */
final class Drawing
{
    /**
     * this private constructor and changing the class to final is to resolve
     * the check-style warning (utility classes should not have a public or
     * default constructor).
     */
    private Drawing ()
    {

    }

    /**
     * @author Mota
     * @param args the command line arguments
     */
    public static void main (String[] args)
    {

        setLookAndFeel();

        SwingUtilities.invokeLater(() ->
        {
            createAndShowGUI();
        });

    }

    /**
     * showing the Graphic user interface.
     */
    private static void createAndShowGUI ()

    {
        JFrame f = new JFrame("Drawing Online");

        f.add(new MainPanel());
        f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        f.pack();
        f.setVisible(true);
    }

    /**
     * setting a Look and feel to the JFrame.
     */
    private static void setLookAndFeel ()
    {
        try
        {
            UIManager.setLookAndFeel("com.formdev.flatlaf.FlatDarkLaf");
        }
        catch (UnsupportedLookAndFeelException | ClassNotFoundException | InstantiationException | IllegalAccessException ex)
        {
            Logger.getLogger(Drawing.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
