/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Point;
import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author mota
 */
public class MovementsMessage implements Serializable
{
    private UUID id;
    private int x;
    private int y;
    private Point offset;
    
    /**
     * 
     */
    public MovementsMessage ()
    {
        
    }
    
    /**
     * 
     * @param id unique id
     * @param x coordinate x
     * @param y coordinate y
     * @param offset point coordinate
     */
    public MovementsMessage (UUID id, int x, int y, Point offset)
    {
        this.id = id;
        this.x = x;
        this.y = y;
        this.offset = offset;
    }
    
    /**
     * 
     * @return id
     */
    public UUID getId ()
    {
        return id;
    }
    
    /**
     * 
     * @param id 
     */
    public void setId (UUID id)
    {
        this.id = id;
    }
    
    /**
     * 
     * @return x
     */
    public int getX ()
    {
        return x;
    }
    
    /**
     * 
     * @param x 
     */
    public void setX (int x)
    {
        this.x = x;
    }
    
    /**
     * 
     * @return y
     */
    public int getY ()
    {
        return y;
    }
    
    /**
     * 
     * @param y 
     */
    public void setY (int y)
    {
        this.y = y;
    }
    
    /**
     * 
     * @return offset
     */
    public Point getOffset ()
    {
        return offset;
    }
    
    /**
     * 
     * @param offset 
     */
    public void setOffset (Point offset)
    {
        this.offset = offset;
    }
    
}
