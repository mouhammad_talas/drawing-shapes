/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Point;
import java.util.UUID;

/**
 *
 * @author mota
 */
/**
 *
 */
public abstract class Shape extends Drawable
{
    private final int xFirstPosition = 10;
    private final int yFirstPosition = 10;
    private Point coordinates;

    private UUID id;

    /**
     *
     */
    public Shape ()
    {
        id = UUID.randomUUID();
    }
    
    /**
     * 
     * @param id 
     */
    public Shape (UUID id)    
    {
        this.id = id;
    }

    /**
     *
     * @return id of Shape
     */
    public UUID getId ()
    {
        return id;
    }

    /**
     *
     * @return coordinates of the Shape
     */
    public Point getCoordinates ()
    {
        return coordinates;
    }
    
    /**
     * to set x and y for the shape.
     * @param x 
     * @param y 
     */
    public void setCoordinates (int x, int y)
    {
        Point coordinatess = new Point(x, y);

        this.coordinates = coordinatess;

    }

    /**
     *
     * @return the x position.
     */
    public int getxFirstPosition ()
    {
        return xFirstPosition;
    }

    /**
     *
     * @return the y position.
     */
    public int getyFirstPosition ()
    {
        return yFirstPosition;
    }

    /**
     * used to move the selected shape.
     *
     * @param x x position
     * @param y y position
     */
    public abstract void move (int x, int y);

    /**
     * used to return true if a shape is selected.
     *
     * @param x x position
     * @param y y position
     * @return true if the shape is selected
     */
    public abstract boolean isSelected (int x, int y);

    /**
     * used to give a color to shapes.
     *
     * @param color the color of shape
     */
    public abstract void setcolor (Color color);

}
