/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.io.Serializable;
import java.util.UUID;

/**
 *
 * @author mota
 */
public class CreateMessage implements Serializable
{
    private UUID id;
    private Class<Shape> shapeClass;
    private int layerIndex;

    /**
     *
     */
    public CreateMessage ()
    {

    }

    /**
     *
     * @param id unique id
     * @param shapeClass class shape
     * @param layerIndex the index of layer
     */
    public CreateMessage (UUID id, Class<Shape> shapeClass, int layerIndex)
    {
        this.id = id;
        this.shapeClass = shapeClass;
        this.layerIndex = layerIndex;
    }

    /**
     *
     * @return id
     */
    public UUID getId ()
    {
        return id;
    }

    /**
     *
     * @param id
     */
    public void setId (UUID id)
    {
        this.id = id;
    }

    /**
     *
     * @return shapeClass
     */
    public Class<Shape> getShapeClass ()
    {
        return shapeClass;
    }

    /**
     *
     * @param shapeClass
     */
    public void setShapeClass (Class<Shape> shapeClass)
    {
        this.shapeClass = shapeClass;
    }

    /**
     *
     * @return layerIndex
     */
    public int getLayerIndex ()
    {
        return layerIndex;
    }

    /**
     *
     * @param layerIndex
     */
    public void setLayerIndex (int layerIndex)
    {
        this.layerIndex = layerIndex;
    }

}
