/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.UUID;

/**
 * this class represents the shape: circle with its attribute and methods.
 * 
 */
public class Circle extends Shape
{
   private int circleX = getxFirstPosition();
   private int circleY = getyFirstPosition();
   private final int circleW = 100;
   private final int circleH = 100;
   private Color currentColor;
   
   
   /**
    * 
    */
   public Circle ()
   {
      super();
   }
   
   /**
    * 
    * @param id unique id 
    */
   public Circle (UUID id)
   {
      super(id);
   }
   
   /**
    * 
    * @return the position of x 
    */
    public int getCircleX ()
    {
        return circleX;
    }

    /**
     * 
     * @param circleX represents the position of x 
     */
    public void setCircleX (int circleX)
    {
        this.circleX = circleX;
    }
    /**
     * 
     */
    public void setX ()
    {
        this.circleX = getCoordinates().x;
    }
    /**
     * 
     */
    public void setY () 
    {
        this.circleY = getCoordinates().y;
    }

    /**
     * 
     * @return the position of y
     */
    public int getCircleY ()
    {
        return circleY;
    }
    
    /**
     * 
     *@param circleY represents the position of y
     */
    public void setCircleY (int circleY)
    {
        this.circleY = circleY;
    }

    /**
     * 
     * @return the width of circle
     */
    public int getCircleW ()
    {
        return circleW;
    }
    
       
/**
 * 
 * @return the height of circle
 */
    public int getCircleH ()
    {
        return circleH;
    }

    
    @Override
    public boolean isSelected (int x, int y)
    {
        double rx = circleW / 2.0;   // horizontal radius of ellipse
        double ry = circleH / 2.0;  // vertical radius of ellipse 
        double cx = circleX + rx;   // x-coord of center of ellipse
        double cy = circleY + ry;    // y-coord of center of ellipse
       return (ry * (x - cx)) * (ry * (x - cx)) + (rx * (y - cy)) * (rx * (y - cy)) <= rx * rx * ry * ry;
    }

    @Override
    public void move (int x, int y)
    {
        circleX += x;
        circleY += y;

        setCoordinates(getCircleX(), getCircleY());

    }

    @Override
    public void draw (Graphics g)
    {

        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(currentColor);
        g2.fillOval(circleX, circleY, circleW, circleH);
        g2.setColor(Color.black);
        g2.drawOval(circleX, circleY, circleW, circleH);

    }

    @Override
    public void setcolor (Color color)
    {
        currentColor = color;
    }

}