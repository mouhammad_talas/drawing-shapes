/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Graphics;
import java.io.Serializable;

/**
 *
 */
public abstract class Drawable implements Serializable
{
    /**
     * to drawing shapes.
     *
     * @param g Graphics
     */
    public abstract void draw (Graphics g);
}
