/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.util.UUID;

/**
 *
 */
public class Diamond extends Shape
{
    private final int x1 = 70;
    private final int x2 = 130;
    private final int x3 = x1;
    private final int x4 = 10;

    private final int y1 = 10;
    private final int y2 = 90;
    private final int y3 = 170;
    private final int y4 = y2;

    private int[] diamondX =
    {
        x1, x2, x3, x4
    };
    private int[] diamondY =
    {
        y1, y2, y3, y4
    };

    private Color currentColor;

    /**
     *
     */
    public Diamond ()
    {
        super();
    }

    /**
     *
     * @param id
     */
    public Diamond (UUID id)
    {
        super(id);
    }

    /**
     *
     * @param diamondX
     */
    public void setDiamondX (int[] diamondX)
    {
        this.diamondX = diamondX;
    }

    /**
     *
     * @param diamondY
     */
    public void setDiamondY (int[] diamondY)
    {
        this.diamondY = diamondY;
    }

    /**
     *
     * @return x position
     */
    public int[] getDiamondX ()
    {
        return diamondX;
    }

    /**
     *
     * @param diamondX x position
     * @param index array index
     */
    public void setDiamondX (int diamondX, int index)
    {
        this.diamondX[index] = diamondX;
    }

    /**
     *
     * @return y position
     */
    public int[] getDiamondY ()
    {
        return diamondY;
    }

    /**
     *
     * @param diamondY y position
     * @param index array index
     */
    public void setDiamondY (int diamondY, int index)
    {
        this.diamondY[index] = diamondY;
    }

    /**
     *
     * @return the diamond instance of General Path
     */
    public GeneralPath createTheDiamond ()
    {
        GeneralPath diamond = new GeneralPath(GeneralPath.WIND_NON_ZERO, diamondX.length);

        diamond.moveTo(diamondX[0], diamondY[0]);
        for (int i = 1; i < diamondX.length; i++)
        {
            diamond.lineTo(diamondX[i], diamondY[i]);
        }
        diamond.closePath();
        return diamond;
    }

    @Override
    public void move (int x, int y)
    {
        for (int i = 0; i < diamondX.length; i++)
        {
            diamondX[i] += x;
            diamondY[i] += y;

        }
    }

    @Override
    public boolean isSelected (int x, int y)
    {
        return createTheDiamond().contains(x, y);

    }

    @Override
    public void draw (Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(currentColor);
        g2.fill(createTheDiamond());
        g2.setColor(Color.black);
        g2.draw(createTheDiamond());

    }

    @Override
    public void setcolor (Color color)
    {
        currentColor = color;
    }

}
