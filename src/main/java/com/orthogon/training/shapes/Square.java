/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.util.UUID;


/**
 *
 */
public class Square extends Shape
{
    private int squareX = getxFirstPosition();
    private int squareY = getyFirstPosition();
    private final int squareW = 100;
    private final int squareH = 100;
    private Color currentColor;
    
    /**
     * 
     */
    public Square ()
   {
      super();
   }
   
    /**
     * 
     * @param id 
     */
   public Square (UUID id)
   {
      super(id);
   }

    /**
     * 
     * @return the position of x
     */
    public int getSquareX ()
    {
        return squareX;
    }

    /**
     * 
     * @param squareX represents the position of x 
     */
    public void setSquareX (int squareX)
    {
        this.squareX = squareX;
    }

    /**
     * 
     * @return the position of y
     */
    public int getSquareY ()
    {
        return squareY;
    }

    /**
     * 
     * @param squareY represents the position of y
     */
    public void setSquareY (int squareY)
    {
        this.squareY = squareY;
    }

    /**
     * 
     * @return the width
     */
    public int getSquareW ()
    {
        return squareW;
    }
    
    /**
     * 
     * @return the height
     */
    public int getSquareH ()
    {
        return squareH;
    }


   
    
    @Override
    public boolean isSelected (int x, int y)
    {
        
        return x <= squareX + squareW && y <= squareY + squareH && x >= squareX && y >= squareY;
    }

    @Override
    public void move (int x, int y)
    {
        squareX += x;
        squareY += y;
        
        setCoordinates(getSquareX(), getSquareY());
        
    }

    @Override
    public void draw (Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;
        
        g2.setColor(currentColor);
        g2.fillRect(squareX, squareY, squareW, squareH);
        g2.setColor(Color.black);
        g2.drawRect(squareX, squareY, squareW, squareH);
        
    }

    @Override
    public void setcolor (Color color)
    {
        currentColor = color;
    }

    
    
   

}
