/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.geom.GeneralPath;
import java.util.UUID;

/**
 *
 */
public class Triangle extends Shape
{
    
    private final int x1 = 70;
    private final int x2 = 130;
    private final int x3 = 10;
    
    private final int y1 = 10;
    private final int y2 = 110;
    private final int y3 = y2;
    
    /**
     * 
     */
    public Triangle ()
   {
      super();
   }
    
   /**
    * 
    * @param id 
    */
   public Triangle (UUID id)
   {
      super(id);
   }
    /**
     *
     */
    private int[] triangleX =
    {
        x1, x2, x3
    };
    /**
     *
     */
    private int[] triangleY =
    {
        y1, y2, y3
    };

    /**
     *
     */
    private Color currentColor;

    /**
     * 
     * @param triangleX x position
     */
    public void setTriangleX (int[] triangleX)
    {
        this.triangleX = triangleX;
    }

    /**
     * 
     * @param triangleY y position
     */
    public void setTriangleY (int[] triangleY)
    {
        this.triangleY = triangleY;
    }
    
    
    /**
     *
     * @return triangleX
     */
    public int[] getTriangleX ()
    {
        return triangleX;
    }
    

    /**
     *
     * @return triangleY
     */
    public int[] getTriangleY ()
    {
        return triangleY;
    }

    /**
     *
     * @param triangleY y position
     * @param index array index 
     */
    public void setTriangleY (int triangleY, int index)
    {
        
           this.triangleY[index] = triangleY; 
        
        
    }
    
    /**
     * 
     * @param triangleX y position
     * @param index array index
     */
    public void setTriangleX (int triangleX, int index)
    {
       
           this.triangleX[index] = triangleX; 
        
        
    }

    /**
     *
     * @return triangle
     */
    public GeneralPath createTheTriangle ()
    {
        GeneralPath triangle = new GeneralPath(GeneralPath.WIND_NON_ZERO, triangleX.length);

        triangle.moveTo(triangleX[0], triangleY[0]);
        for (int i = 1; i < triangleX.length; i++)
        {
            triangle.lineTo(triangleX[i], triangleY[i]);
        }
        triangle.closePath();
        return triangle;
    }

    @Override
    public void move (int x, int y)
    {
        for (int i = 0; i < triangleX.length; i++)
        {
            triangleX[i] += x;
            triangleY[i] += y;

        }
    }

    @Override
    public boolean isSelected (int x, int y)
    {
        return createTheTriangle().contains(x, y);

    }

    @Override
    public void draw (Graphics g)
    {
        Graphics2D g2 = (Graphics2D) g;

        g2.setColor(currentColor);
        g2.fill(createTheTriangle());
        g2.setColor(Color.BLACK);
        g2.draw(createTheTriangle());
    }

    @Override
    public void setcolor (Color color)
    {
        currentColor = color;
    }

}
