package com.orthogon.training.shapes;

import java.awt.Color;
import java.awt.Graphics;
import java.util.Hashtable;
import java.util.Set;
import java.util.UUID;

/**
 *
 */
public class Layer extends Drawable

{
    private Hashtable<UUID, Shape> elements = new Hashtable<>();

    /**
     *
     * @return a list of shapes.
     */
    public Hashtable<UUID, Shape> getElements ()
    {

        return elements;
    }

    /**
     *
     * @param elements
     */
    public void setElements (Hashtable<UUID, Shape> elements)
    {
        this.elements = elements;
    }

    @Override
    public void draw (Graphics g)
    {
        elements.forEach((key, element) ->
        {
            element.draw(g);
        });
    }

    /**
     *
     * @param element
     */
    public void add (Shape element)
    {
        elements.put(element.getId(), element);
    }

    /**
     *
     * @param element
     */
    public void remove (Shape element)
    {
        elements.remove(element);
    }

    /**
     *
     * @param color
     */
    public void setColor (Color color)
    {
        elements.forEach((key, element) ->
        {
            element.setcolor(color);
        });
    }

    /**
     *
     * @param x represents the x position.
     * @param y represents the y position.
     * @return gets the selected shape.
     */
    public Shape select (int x, int y)
    {
        Shape selectedShape = null;

        Set<UUID> keys = elements.keySet();

        for (UUID key : keys)
        {
            if (elements.get(key).isSelected(x, y))
            {
                selectedShape = elements.get(key);
            }
        }

        return selectedShape;
    }

}
